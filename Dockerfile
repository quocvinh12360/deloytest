FROM node:14.15

ARG PORT

RUN mkdir -p /usr/src/deloy_test

WORKDIR /usr/src/deloy_test

COPY package*.json ./

RUN npm config set package-lock false
RUN npm install
RUN npm audit fix
RUN echo "Your app is running on PORT: ${PORT}"
COPY . .

EXPOSE $PORT

CMD ["npm", "start"]
